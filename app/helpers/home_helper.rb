# frozen_string_literal: true

module HomeHelper
  def display_seeds_hint
    return unless Flipper.enabled?(:demo)
    return if User.exists?

    content_tag(:h1, 'Welcome to Blind Meetings app! Please, run `rails db:seed` and reload the page.')
  end

  def display_generate_hint(groups)
    return unless Flipper.enabled?(:demo)
    return if groups.present?

    content_tag(:h1, 'Yay! You can generate the very first Activity!')
  end
end
