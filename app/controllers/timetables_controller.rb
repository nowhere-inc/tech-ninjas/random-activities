# frozen_string_literal: true

class TimetablesController < ApplicationController
  def show
    timetable = TimetableGenerationService.new(from: params[:from], to: params[:to]).call
    if timetable.valid?
      render json: timetable
    else
      render json: { errors: timetable.errors.full_messages }, status: :bad_request
    end
  end
end
