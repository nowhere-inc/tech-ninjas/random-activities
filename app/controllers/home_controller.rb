# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @groups = Group.includes(:members).all
  end
end
