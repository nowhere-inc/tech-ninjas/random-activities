# frozen_string_literal: true

class TimestampsController < ApplicationController
  def create
    render_event_error and return unless Timestamp.events.keys.include?(params[:event])

    timestamp = Timestamp.new(timestamp_params)
    if timestamp.save
      render(json: timestamp, status: :created)
    else
      render(json: { errors: timestamp.errors.full_messages }, status: :created)
    end
  end

  private

  def render_event_error
    render(
      json: { errors: ["'event' value is not valid. Valid values are: #{Timestamp.events.keys.join(', ')}"] },
      status: :bad_request
    )
  end

  def timestamp_params
    params.permit(:name, :activity, :event).merge(timestamp: parse_timestamp)
  end

  def parse_timestamp
    Time.at(params[:timestamp].to_i).utc.to_datetime
  end
end
