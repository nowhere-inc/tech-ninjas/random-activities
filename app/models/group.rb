# frozen_string_literal: true

class Group < ApplicationRecord
  MULTIPLE_STEWARDS_ERROR = 'Only one steward should be assigned for a group'

  belongs_to :activity
  has_many :group_memberships, dependent: :destroy
  has_many :members, through: :group_memberships, source: :user, class_name: 'User'
  has_one :steward_membership, -> { where(steward: true) }, inverse_of: :group, class_name: 'GroupMembership',
                                                            dependent: :destroy
  has_one :steward, source: :user, class_name: 'User', through: :steward_membership

  validate :single_steward

  private

  def single_steward
    errors.add(:base, MULTIPLE_STEWARDS_ERROR) if group_memberships.count(&:steward?) > 1
  end
end
