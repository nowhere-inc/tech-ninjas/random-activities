# frozen_string_literal: true

class Timestamp < ApplicationRecord
  enum event: { start: 0, finish: 1 }
end
