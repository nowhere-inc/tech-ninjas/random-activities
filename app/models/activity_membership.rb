# frozen_string_literal: true

class ActivityMembership < ApplicationRecord
  belongs_to :user
  belongs_to :activity
end
