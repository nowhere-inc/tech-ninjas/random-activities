# frozen_string_literal: true

class Activity < ApplicationRecord
  has_many :groups, dependent: :destroy
  has_many :activity_memberships, dependent: :destroy
  has_many :participants, through: :activity_memberships, source: :user, class_name: 'User'
  has_many :messages, as: :messageable, dependent: :destroy
end
