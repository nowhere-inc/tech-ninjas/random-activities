# frozen_string_literal: true

class User < ApplicationRecord
  belongs_to :team
  has_many :group_memberships, dependent: :destroy
  has_many :activity_memberships, dependent: :destroy
  has_many :activities, through: :activity_memberships
  has_many :groups, through: :group_memberships
  has_many :messages, inverse_of: :user, dependent: :destroy
end
