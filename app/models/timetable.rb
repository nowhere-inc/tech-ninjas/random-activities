# frozen_string_literal: true

# Timetable represents the actual timetable, validate data and implement timetable methods
class Timetable
  include Enumerable

  def initialize(timestamps:)
    @items = collect_items(timestamps)
  end

  def valid?
    all?(&:valid?)
  end

  def as_json(*_args)
    map(&:as_json)
  end

  private

  attr_reader :items

  def each(&)
    items.each(&)
  end

  def collect_items(timestamps)
    timestamps.pluck(:name, :timestamp, :event)
              .group_by { |entry| entry[0] }
              .map { |name, data| TimetableItem.new(name:, data:) }
  end
end
