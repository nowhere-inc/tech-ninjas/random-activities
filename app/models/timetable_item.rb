# frozen_string_literal: true

# TimetableItem represents the timetable item, validate data and implement rails-friendly methods
class TimetableItem
  include ActiveModel::Validations

  def initialize(name:, data:)
    @data = data
    @name = name
  end

  def as_json
    { name:, total_hours: }
  end

  def valid?
    true
  end

  private

  attr_reader :data, :name

  def total_hours
    data.group_by { |entry| entry[1].strftime('%Y-%m-%d') }
        .sum do |_date, values|
          start_item = values.detect { |item| item[2] == 'start' }
          finish_item = values.detect { |item| item[2] == 'finish' }
          if start_item && finish_item
            (finish_item[1] - start_item[1]) / 3600
          else
            0
          end
        end
  end
end
