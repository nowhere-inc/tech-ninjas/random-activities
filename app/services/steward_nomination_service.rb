# frozen_string_literal: true

# Forms groups from existing users based on group_size provided
class StewardNominationService
  INVALID_USERS_ARGUMENT = 'No users for groups fromation'

  def initialize(formed_group:)
    @formed_group = formed_group
  end

  def call
    nominate_steward
  end

  private

  attr_reader :formed_group

  def nominate_steward
    formed_group.sample
  end
end
