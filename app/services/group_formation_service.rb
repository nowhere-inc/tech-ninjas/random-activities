# frozen_string_literal: true

# Forms groups from existing users based on group_size provided
class GroupFormationService
  INVALID_USERS_ARGUMENT = 'No users for groups fromation'

  def initialize(group_size: 5, users: User.all)
    @group_size = group_size
    @users = users
    @formed_groups = []
  end

  def call
    validate_arguments!
    form_groups
  end

  private

  attr_reader :group_size, :users, :formed_groups

  def validate_arguments!
    raise INVALID_USERS_ARGUMENT if users.empty?
  end

  def single_group_should_be_formed?
    users.size <= ((group_size * 2) - 2)
  end

  def form_groups
    return [users] if single_group_should_be_formed?

    larger_groups_quantity.times { grab_users_sample!(group_size + 1) }
    smaller_groups_quantity.times { grab_users_sample!(group_size) }

    formed_groups
  end

  def grab_users_sample!(sample_size)
    current_group = users.sample(sample_size)
    @users -= current_group

    @formed_groups << current_group
  end

  def smaller_groups_quantity
    (users.size / group_size) - larger_groups_quantity
  end

  def larger_groups_quantity
    (users.size % group_size)
  end
end
