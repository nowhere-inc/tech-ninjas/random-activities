# frozen_string_literal: true

# TimetableGenerationService fetches range of timestamps and instantiate Timetable
class TimetableGenerationService
  def initialize(from:, to:)
    @from = from
    @to = to
  end

  def call
    Timetable.new(timestamps:)
  end

  private

  attr_reader :from, :to

  def timestamps
    Timestamp.where(timestamp: range)
  end

  def range
    DateTime.parse(from).utc.beginning_of_day..DateTime.parse(to).utc.end_of_day
  end
end
