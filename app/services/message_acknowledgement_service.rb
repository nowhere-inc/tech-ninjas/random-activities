# frozen_string_literal: true

# Inserts provided groups to the db
class MessageAcknowledgementService
  def initialize(message:)
    @message = message
  end

  def call
    message.acknowledged = true
    message.save!
  end

  private

  attr_reader :message
end
