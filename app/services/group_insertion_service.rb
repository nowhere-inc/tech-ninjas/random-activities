# frozen_string_literal: true

# Inserts provided groups to the db
class GroupInsertionService
  INVALID_GROUPS_ARGUMENT = 'An array of groups to insert is expected'
  ACTIVITY_MEMBERSHIP_ABSENT = 'Activity membership should be in place to add user to the group'

  def initialize(activity:, groups: [])
    @groups = groups
    @activity = activity
  end

  def call
    validate_arguments!
    populate_groups!
  end

  private

  attr_reader :groups, :activity

  def validate_arguments!
    raise INVALID_GROUPS_ARGUMENT unless groups.all? { |group| group.is_a?(Array) }
    raise ACTIVITY_MEMBERSHIP_ABSENT unless groups.flatten.all? { |user| user.activities.include?(activity) }
  end

  def populate_groups!
    groups.each do |group_users|
      group = Group.create(activity:)
      group_users.each { |user| GroupMembership.create(user:, group:) }
    end
  end
end
