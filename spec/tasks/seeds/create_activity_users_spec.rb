# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Seeds::CreateActivityUsers do
  subject(:create_users) { described_class.new(quantity:, activity:) }

  let(:activity) { create(:activity) }
  let(:quantity) { 1 }

  it 'creates users' do
    expect { create_users.call }.to change(User, :count).from(0).to(quantity)
  end

  it 'creates activity users' do
    expect { create_users.call }.to change { activity.participants.count }.from(0).to(quantity)
  end

  it 'creates messages' do
    expect { create_users.call }.to change(Message, :count).from(0).to(quantity)
  end
end
