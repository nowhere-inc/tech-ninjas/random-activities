# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Seeds::CleanupEnvironment do
  subject(:cleaner) { described_class.new }

  it 'does nothing' do
    expect { cleaner.call }.not_to raise_error
  end

  context 'with existing data' do
    before { create(:defined_activity) }

    it 'deletes existing user records' do
      expect { cleaner.call }.to change(User, :count).to(0)
    end

    it 'deletes existing group records' do
      expect { cleaner.call }.to change(Group, :count).to(0)
    end

    it 'deletes existing group_membership records' do
      expect { cleaner.call }.to change(GroupMembership, :count).to(0)
    end

    it 'deletes existing activities records' do
      expect { cleaner.call }.to change(Activity, :count).to(0)
    end

    it 'deletes existing message records' do
      expect { cleaner.call }.to change(Message, :count).to(0)
    end

    it 'deletes existing teams records' do
      expect { cleaner.call }.to change(Team, :count).from(1).to(0)
    end
  end
end
