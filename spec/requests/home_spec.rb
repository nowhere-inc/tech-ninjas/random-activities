# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Groups' do
  describe 'GET /' do
    context 'when there is no users yet' do
      it 'displays the hint' do
        get '/'
        expect(response).to have_http_status(:success)
        expect(response.body).to include('Please, run `rails db:seed` and reload the page.')
      end
    end

    context 'when there are no groups' do
      before { create(:user) }

      it 'displays the hint' do
        get '/'
        expect(response).to have_http_status(:success)
        expect(response.body).to include('Yay! You can generate the very first Activity!')
      end
    end

    context 'when groups exist' do
      before { create(:defined_activity) }

      it 'displays the list of things' do
        get '/'
        expect(response).to have_http_status(:success)
        expect(response.body).to include('Group #')
      end
    end
  end
end
