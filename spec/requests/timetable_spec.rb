# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Timetable' do
  def compose(timestamp:)
    Time.parse(timestamp).to_i
  end

  def post_timelog(name:, time:, event:)
    post('/timestamps',
         params: { activity: 'Defined', name:, timestamp: compose(timestamp: time), event: })
  end

  def get_timetable(from: '2023-07-01', to: '2023-07-30')
    get("/timetable?from=#{from}&to=#{to}")
    response.parsed_body.map(&:symbolize_keys)
  end

  def generate_events!
    post_timelog(name: 'Busy Organizer', time: '2023-07-01 16:00', event: :finish)
    post_timelog(name: 'Busy Organizer', time: '2023-07-02 08:00', event: :start)
    post_timelog(name: 'Busy Organizer', time: '2023-07-02 16:00', event: :finish)
    post_timelog(name: 'Busy Organizer', time: '2023-07-01 08:00', event: :start)
  end

  it 'generates timetable' do
    generate_events!
    timetable = get_timetable

    expect(timetable).to include({ name: 'Busy Organizer', total_hours: 16.0 })
  end

  describe 'POST /timetable' do
    context 'with invalid event' do
      it 'returns 400 code' do
        post_timelog(name: 'Busy Organizer', time: '2023-07-01 16:00', event: :unknown)

        expect(response).to have_http_status(:bad_request)
      end
    end
  end
end
