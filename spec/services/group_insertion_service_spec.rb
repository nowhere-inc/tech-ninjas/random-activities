# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GroupInsertionService do
  subject(:service) { described_class.new(activity:, groups:) }

  let(:activity) { create(:activity) }

  context 'when argument is not an array of groups' do
    subject(:service) { described_class.new(activity:, groups: %i[some array]) }

    it 'raises an error' do
      expect { service.call }.to raise_error(described_class::INVALID_GROUPS_ARGUMENT)
    end
  end

  context 'with existing valid users' do
    let(:users) { create_list(:user, 2, activities: [activity]) }
    let(:groups) { [users] }

    it 'creates group records' do
      expect { service.call }.to change(Group, :count).from(0).to(1)
    end

    it 'creates group_membership records' do
      expect { service.call }.to change(GroupMembership, :count).from(0).to(2)
    end
  end

  context 'when activity membership is absent' do
    let(:random_user) { create(:user) }
    let(:activity_member) { create(:user, activities: [activity]) }
    let(:groups) { [[activity_member, random_user]] }

    it 'rises error' do
      expect { service.call }.to raise_error(described_class::ACTIVITY_MEMBERSHIP_ABSENT)
    end
  end
end
