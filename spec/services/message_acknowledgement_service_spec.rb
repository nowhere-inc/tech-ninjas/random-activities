# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessageAcknowledgementService do
  subject(:service) { described_class.new(message:) }

  let(:message) { create(:message) }

  it 'marks message as acknowledged' do
    expect { service.call }.to change(message, :acknowledged?).from(false).to(true)
  end
end
