# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StewardNominationService do
  context 'with default arguments' do
    subject(:service) { described_class.new(formed_group:) }

    let(:activity) { build(:defined_activity) }
    let(:formed_group) { activity.groups.last.members }

    it 'nominates steward' do
      expect(service.call).to be_a(User)
    end
  end
end
