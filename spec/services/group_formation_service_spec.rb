# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GroupFormationService do
  context 'with default arguments' do
    context 'when user records are absent' do
      subject(:service) { described_class.new }

      it 'raises an error' do
        expect { service.call }.to raise_error(described_class::INVALID_USERS_ARGUMENT)
      end
    end

    context 'when small amount of user records exist' do
      subject(:service_result) { described_class.new.call }

      before do
        create_list(:user, 3)
      end

      it 'gathers everyone' do
        expect(service_result.first.size).to eq User.count
      end

      it 'forms a single group' do
        expect(service_result.size).to eq 1
      end
    end
  end

  context 'with provided objects' do
    shared_examples 'valid group formation from teams' do |group_size: 5, teams_quantity: 3|
      subject(:service_result) { described_class.new(group_size:, users:).call }

      let(:teams) { build_list(:team, teams_quantity) }
      let(:users) { teams.map { |team| build_list(:user, rand(4..12), team:) }.flatten }
      let(:group_sizes) { [group_size, group_size + 1] }

      it 'forms groups of provoded size' do
        expect(service_result.map(&:size).uniq - group_sizes).to be_empty
      end

      it 'gathers everyone' do
        expect(service_result.flatten - users).to be_empty
      end

      it "spreads #{teams_quantity} teams' members into groups of #{group_size}" do
        teams_for_groups = service_result.map { |group_members| group_members.map { |user| user.team.name }.uniq.size }
        expect(teams_for_groups).to all(be_in(teams_quantity - 2..teams_quantity))
      end
    end

    # flacky specs. Please check https://gitlab.com/nowhere-inc/tech-ninjas/random-activities/-/issues/52
    # it_behaves_like 'valid group formation from teams', group_size: 3, teams_quantity: 2
    # it_behaves_like 'valid group formation from teams', group_size: 5, teams_quantity: 5
    # it_behaves_like 'valid group formation from teams', group_size: 7, teams_quantity: 6
    # it_behaves_like 'valid group formation from teams', group_size: 8, teams_quantity: 5
  end
end
