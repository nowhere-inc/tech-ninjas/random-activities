# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Group do
  describe 'associations' do
    subject(:group) { build(:group, :with_members, :with_steward) }

    let(:group_steward) { group.group_memberships.where(steward: true).last&.user }
    let(:random_participant) { [group.members - Array.wrap(group_steward)].sample }
    let(:random_participant_membership) { group.group_memberships.where(user: random_participant).last&.user }

    describe '#steward' do
      it 'returns steward' do
        expect(group.steward).to eq(group_steward)
      end
    end
  end

  describe 'validations' do
    subject(:group) { build(:group, :with_steward) }

    context 'with more than one steward' do
      before { group.group_memberships << build(:group_membership, :steward) }

      it { is_expected.not_to be_valid }

      it 'contains correct error message' do
        group.validate
        expect(group.errors[:base]).to contain_exactly('Only one steward should be assigned for a group')
      end
    end
  end
end
