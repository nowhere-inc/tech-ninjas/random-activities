# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    user factory: :user
    messageable factory: :activity
  end
end
