# frozen_string_literal: true

FactoryBot.define do
  factory :group do
    activity

    trait :with_members do
      after(:build) { |group| group.group_memberships << build_list(:group_membership, 2) }
    end

    trait :with_steward do
      after(:build) { |group| group.group_memberships << build(:group_membership, :steward) }
    end
  end
end
