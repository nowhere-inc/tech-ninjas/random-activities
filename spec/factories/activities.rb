# frozen_string_literal: true

FactoryBot.define do
  factory :activity do
    name { 'Defined' }
  end

  factory :defined_activity, parent: :activity do
    after(:build) do |activity|
      group = build(:group)
      user = build(:user)
      user.messages.build(messageable: activity)
      group.members << user
      activity.groups << group
      activity.participants << user
    end
  end
end
