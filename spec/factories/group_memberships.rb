# frozen_string_literal: true

FactoryBot.define do
  factory :group_membership do
    user

    trait :steward do
      steward { true }
    end
  end
end
