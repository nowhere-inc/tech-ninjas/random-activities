# frozen_string_literal: true

module Seeds
  # Deletes existing data before seeding
  class CleanupEnvironment
    def call
      [GroupMembership, Group, ActivityMembership, Message, Activity, User, Team].each(&:delete_all)
    end
  end
end
