# frozen_string_literal: true

module Seeds
  # Creates seed users
  class CreateActivityUsers
    def initialize(activity:, quantity: rand(30..40))
      @activity = activity
      @quantity = quantity
    end

    def call
      create_team_and_users!
    end

    private

    attr_reader :activity, :quantity

    def create_team_and_users!
      team = Team.create(name: Faker::Team.mascot)
      quantity.times do
        attrs = { team:, first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, activities: [activity] }
        user = User.create!(attrs)
        user.messages.create(messageable: activity, body: "Activity #{activity.name} is open for participation.")
      end
    end
  end
end
