# frozen_string_literal: true

Dir[Rails.root.join('db/seeds/*.rb')].each do |seed|
  require seed
end

# rubocop:disable Rails/Output
puts 'Deleting existing data'
Seeds::CleanupEnvironment.new.call

puts 'Turning :demo features ON'
Flipper.enable :demo

puts 'Creating random quantity of User records with activity membership'
activity = Activity.create(name: 'Undefined')
Seeds::CreateActivityUsers.new(activity:).call

puts 'Creating the activity and groups of equal size (+/- 1 person)'
GroupInsertionService.new(
  activity:,
  groups: GroupFormationService.new.call
).call
# rubocop:enable Rails/Output
