# frozen_string_literal: true

class CreateMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :messages do |t|
      t.text :body
      t.references :user, null: false, foreign_key: true
      t.references :messageable, polymorphic: true, null: false, index: true
      t.boolean :acknowledged, null: false, default: false

      t.timestamps
    end
  end
end
