# frozen_string_literal: true

class CreateActivityMemberships < ActiveRecord::Migration[7.0]
  def change
    create_table :activity_memberships do |t|
      t.references :user, null: false, foreign_key: true
      t.references :activity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
