# frozen_string_literal: true

class CreateTimestamps < ActiveRecord::Migration[7.0]
  def change
    create_table :timestamps do |t|
      t.string :activity
      t.string :name
      t.datetime :timestamp
      t.integer :event

      t.timestamps
    end
  end
end
