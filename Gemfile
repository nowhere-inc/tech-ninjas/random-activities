# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

gem 'pg', '~> 1.5'
gem 'puma', '~> 6.4'
gem 'rails', '~> 7.1'
gem 'redis', '~> 5.0'
gem 'sassc-rails', '~> 2.1'
gem 'sidekiq', '~> 7.1'
gem 'sqlite3', '~> 1.6'

group :development do
  gem 'rack-mini-profiler', '~> 3.1'
end

group :development, :test do
  gem 'bullet', '~> 7.1'
  gem 'factory_bot_rails', '~> 6.2'
  gem 'faker', '~> 3.2'
  gem 'flipper', '~> 1.0'
  gem 'flipper-active_record', '~> 1.0'
  gem 'listen', '~> 3.8'
  gem 'pry-rails', '~> 0.3.9'
  gem 'rspec-rails', '~> 6.0'
  gem 'rubocop-rails', '~> 2.21'
  gem 'rubocop-rspec', '~> 2.24'
  gem 'spring', '~> 4.1'
  gem 'strong_versions', '~> 0.4.5'
end

group :test do
  gem 'database_cleaner-active_record', '~> 2.1'
end

gem 'dockerfile-rails', '~> 1.5'

gem 'sentry-ruby', '~> 5.12'

gem 'sentry-rails', '~> 5.12'
