# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'

  get 'home/index'
  resource :timetable, only: :show
  resources :timestamps, only: :create
end
