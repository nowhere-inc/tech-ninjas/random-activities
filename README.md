### random activities are here to automate the generation of groups for cool activities based on simple settings.


### Dev env setup with no docker. Requires `asdf`, `ruby 3.2.2`, `bundler` and `postgreSQL`

All dependencies are expected to be available in the system. To install that, [a brief guide from GoRails](https://gorails.com/setup) can be used.

The steps to run an app locally after clonning:
```
bundle

rails db:crete db:migrate

rails db:seed

rails s
```

### Semi-production is at [https://alkoretreat.fly.dev/](https://alkoretreat.fly.dev/)
